<?php
/**
 * Template Name: Quartier
 * Description: A Page Template for the contact page.
 */
get_header();
?>


    <div id="wrapper" class="page-content">
        <div class="content-holder">

            <!-- content  -->
            <!-- Page title -->
            <div class="dynamic-title"><h1><?php the_title() ?></h1></div>
            <!-- Page title  end-->
            <!-- content  -->
            <div class="content background-header" style="background-image:url('<?php the_field('quartier_background_image'); ?>')">
                <div class="overlay"></div>
                <section>
                    <div class="container">
                        <div class="page-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?php the_title() ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="quartier-video">
                                <h2 class="section-title"><?php the_field('field_quartier_title'); ?></h2>
                                <p><?php the_field('field_quartier_text'); ?></p>
                            </div>
                            <div class="module-video-thumbnail">
                                <div id="thumbnail" class="d-flex align-items-center justify-content-center"
                                     style="background-image: url('<?php the_field('field_quartier_thumbnail'); ?>');">
                                    <?php
                                    $videoID = "";
                                    $videoURL = get_field('field_quartier_video');
                                    if (strpos($videoURL, 'www.youtube.com') !== false) {
                                        $videoID = explode('?v=', $videoURL)[1];
                                    } else {
                                        $videoID = explode('.be/', $videoURL)[1];
                                    }
                                    ?>

                                    <div class="media-video__video"
                                         data-videoid="<?php echo $videoID ?>"></div>
                                    <a class="video-play-trigger"><img src="<?php echo get_template_directory_uri() ?>/assets/svg/play-btn.svg" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content quartier-map">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 background-darkerblue">
                            <h2 class="section-title white"><?php the_field('field_quartier_title_map');?></h2>
                            <?php the_field('field_quartier_list');?>
                        </div>
                        <div class="col-xl-8 col-lg-6 p-0">
                            <div id="googleMap" style="width:100%;height:600px;"></div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include 'templates/includes/content-footer.php'; ?>
            <!-- content end -->
        </div>

    </div><!-- .content-area -->

<?php
get_footer();
