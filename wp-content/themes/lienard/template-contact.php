<?php
/**
 * Template Name: Contact
 * Description: A Page Template for the contact page.
 */
get_header();
?>


    <div id="wrapper" class="page-content">
        <div class="content-holder">

            <!-- content  -->
            <!-- Page title -->
            <div class="dynamic-title"><h1><?php the_title() ?></h1></div>
            <!-- Page title  end-->
            <!-- content  -->
            <div class="content background-header" style="background-image:url('<?php the_field('contact_background_image'); ?>')">
                <div class="overlay"></div>
                <section>
                    <div class="container">
                        <div class="page-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?php the_title() ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content contact-info">
                <div class="container">
                    <div class="row presentation-first align-items-center">
                        <div class="col-lg-7">
                            <h2 class="section-title"><?php the_field('field_contact_informations_title'); ?></h2>
                            <div class="informations">
                                <div>
                                    <h5><?php _e('Téléphone', 'lienard'); ?></h5>
                                    <ul>
                                        <li>
                                            <a href="tel:<?php the_field('field_footer_telephone', 'option'); ?>"><?php the_field('field_footer_telephone', 'option'); ?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <h5><?php _e('Adresse', 'lienard'); ?></h5>
                                    <p><?php the_field('field_footer_address', 'option') ?></p>
                                </div>
                                <div>
                                    <h5><?php _e('Horaire', 'lienard'); ?></h5>
                                    <p><?php the_field('field_footer_hours', 'option'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div id="googleMap" style="width:100%;height:540px;"></div>
                    </div>
                </div>
            </div>
            <div class="content contact-form">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 pr-lg-5 formulaire">
                                <?php echo do_shortcode('[contact-form-7 id="' . get_field('field_contact_form') . '"]'); ?>
                            </div>
                            <div class="col-lg-5">
                                <div class="sidebar-text">
                                    <h2 class="section-title"><?php the_field('field_contact_sidebar_title'); ?></h2>
                                    <p><?php the_field('field_contact_sidebar_text'); ?></p>
                                    <?php $link = get_field('field_contact_sidebar_button');
                                    if ($link):
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                        ?>
                                        <a class=" btn anim-button   flat-btn   transition"
                                           href="<?php echo esc_url($link_url); ?>"
                                           target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                                    class="fa fa-long-arrow-right"></i></a>
                                    <?php endif; ?>
                                </div>
                                <div class="sidebar-img">
                                    <img src="<?php the_field('field_contact_sidebar_image'); ?>"
                                         alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include 'templates/includes/content-footer.php'; ?>
            <!-- content end -->
        </div>

    </div><!-- .content-area -->

<?php
get_footer();
