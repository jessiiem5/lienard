/**
 * Lists all available gulp tasks.
 */
module.exports = function (gulp, plugins) {

  // Constants specific to the help task.
  const excluded = [
    'assets',
    'clean',
    'concat',
    'data-fallback-concat',
    'data-png-concat',
    'data-svg-concat',
    'icons',
    'pictos',
    'sass',
    'usemin',
  ];

  return function () {
    return plugins.taskListing.withFilters(null, function (task) {
      return excluded.indexOf(task) >= 0;
    })();
  };

};
