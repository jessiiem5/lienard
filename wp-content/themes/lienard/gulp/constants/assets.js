/**
 * Contains all constants related to assets.
 */
module.exports = {
  // Assets paths.
  path: {
    public: './assets', // Directory of where public assets are
    resources: './assets', // Directory of where raw assets are, could be the same as public, depending on framework
    usemin: './templates/includes/header.php', // If used, directory containing files where assets are embedded
    css: '/',
    scss: '/scss',
    sass: '/scss/**/*.scss',
    svg: '/svg/assets/*.svg',
    icons: '/svg/icons/*.svg',
    pictos: '/svg/pictos/*.svg',
    min: '/dist',
    dist: '/dist/output',
    tmp: '/dist/output/*tmp*',
    twig: '/dist/*.twig',
  },
};
