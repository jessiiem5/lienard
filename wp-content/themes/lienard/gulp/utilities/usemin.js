/**
 * Minifies assets files.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');

  return function () {
    return gulp.src(assets.path.usemin)
    .pipe(plugins.usemin({
      html: [],
      css: [plugins.cssmin()],
      js: [plugins.uglify()]
    }))
    .pipe(gulp.dest(assets.path.public + assets.path.min));
  };

};
