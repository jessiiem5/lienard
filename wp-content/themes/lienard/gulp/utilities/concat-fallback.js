/**
 * Concatenates fallback files.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');

  return function () {
    return gulp.src([assets.path.public + assets.concat.fallback.source, '!' + assets.path.public + assets.concat.fallback.target])
    .pipe(gulp.dest(assets.path.public + assets.path.dist));
  };

};
