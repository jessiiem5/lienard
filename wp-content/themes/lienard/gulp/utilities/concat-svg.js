/**
 * Concatenates SVG files.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');

  return function () {
    return gulp.src([assets.path.public + assets.concat.svg.source, '!' + assets.path.public + assets.concat.svg.target])
    .pipe(gulp.dest(assets.path.public + assets.path.dist));
  };

};
