/**
 * Generates pictos files.
 */
module.exports = function (gulp, plugins) {

  // Global constants.
  const assets = require('../constants/assets');

  // Dependencies.
  const gulpicon = require('gulpicon/tasks/gulpicon');
  const glob = require('glob');

  return gulpicon(
    glob.sync(assets.path.public + assets.path.pictos),
    {
      enhanceSVG: true,
      pngfolder: 'pictos',
      datasvgcss: 'pictos.tmp.data.svg.css',
      datapngcss: 'pictos.tmp.data.png.css',
      urlpngcss: 'pictos.tmp.fallback.css',
      cssprefix: '.picto-',
      dest: assets.path.public + assets.path.dist,
    }
  );

};
