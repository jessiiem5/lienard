<?php
/**
 * Template Name: Home
 * Description: A Page Template for the home page.
 */
get_header();
?>


    <div id="wrapper">
        <div class="content-holder">

            <!-- content  -->
            <div class="content full-height" id="sec1">
                <!-- Hero section   -->
                <div class="hero-wrap">
                    <!-- Hero image   -->
                    <div class="bg" data-bg="<?php the_field('field_home_background_image'); ?>"
                         data-top-bottom="transform: translateY(300px);"
                         data-bottom-top="transform: translateY(-300px);"></div>
                    <!-- Hero image   end -->
                    <div class="overlay"></div>
                    <!-- Hero text   -->
                    <div class="hero-wrap-item center-item">
                        <img src="<?php the_field('field_home_logo'); ?>" />
                    </div>
                    <!-- Hero text   end-->
                    <a href="#sec2" class="hero-scroll-link custom-scroll-link"
                       data-top-bottom="transform: translateY(50px);" data-bottom-top="transform: translateY(-50px);"><i
                                class="fa fa-angle-down"></i></a>
                </div>
                <!-- Hero section   end -->
            </div>

            <!-- content  -->
            <div class="content" id="sec2">
                <section>
                    <!-- section number   -->
                    <div class="sect-subtitle right-align-dec" data-top-bottom="transform: translateY(200px);"
                         data-bottom-top="transform: translateY(-200px);"><span>01</span></div>
                    <!-- section number  end  -->
                    <!--  container  -->
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-6">
                                <div class="module-video-thumbnail">
                                    <div id="thumbnail" class="d-flex align-items-center justify-content-center"
                                         style="background-image: url('<?php the_field('field_home_intro_thumbnail'); ?>');">
                                        <?php
                                        $videoID = "";
                                        $videoURL = get_field('field_home_intro_video');
                                        if (strpos($videoURL, 'www.youtube.com') !== false) {
                                            $videoID = explode('?v=', $videoURL)[1];
                                        } else {
                                            $videoID = explode('.be/', $videoURL)[1];
                                        }
                                        ?>

                                        <div class="media-video__video"
                                             data-videoid="<?php echo $videoID ?>"></div>
                                        <a class="video-play-trigger"><img src="<?php echo get_template_directory_uri() ?>/assets/svg/play-btn.svg" /></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <!-- section title  -->
                                <h2 class="section-title"><?php the_field('field_home_intro_title'); ?></h2>
                                <!-- section title  end -->
                                <?php the_field('field_home_intro_text'); ?>
                                <?php $link = get_field('field_home_intro_button');
                                if ($link):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <a class="btn anim-button fl-l" href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                                class="fa fa-long-arrow-right"></i></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!--  container end  -->
                </section>
            </div>
            <div class="content dark-bg">
                <!-- section number   -->
                <div class="sect-subtitle left-align-dec" data-top-bottom="transform: translateY(-200px);"
                     data-bottom-top="transform: translateY(200px);"><span>02</span></div>
                <!-- section number   end -->
                <!-- parallax image  -->
                <div class="parallax-inner">
                    <div class="bg" data-bg="<?php the_field('field_home_counter_background'); ?>"
                         data-top-bottom="transform: translateY(300px);"
                         data-bottom-top="transform: translateY(-300px);"></div>
                    <div class="overlay"></div>
                </div>
                <!-- parallax image  end -->
                <section>
                    <!--  container  -->
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 numbers-title">
                                <h2 class="section-title"><?php the_field('field_home_counter_title'); ?></h2>
                                <?php $link = get_field('field_home_counter_button');
                                if ($link):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <a class="btn anim-button fl-l" href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                                class="fa fa-long-arrow-right"></i></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-6">
                                <p><?php the_field('field_home_counter_text'); ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5"></div>
                            <div class="col-lg-7">
                                <!-- facts   -->
                                <div class="inline-facts-holder row">
                                    <!-- 1 -->
                                    <div class="inline-facts col-lg-4 col-xl-3">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num"
                                                     data-content="<?php the_field('field_home_counter_type'); ?>"
                                                     data-num="<?php the_field('field_home_counter_type'); ?>">0
                                                </div>
                                            </div>
                                        </div>
                                        <h6><?php _e('Types d\'unité', 'lienard'); ?></h6>
                                    </div>
                                    <!-- 3 -->
                                    <div class="inline-facts col-lg-3">
                                        <div class="milestone-counter">
                                            <div class="stats animaper">
                                                <div class="num"
                                                     data-content="<?php the_field('field_home_counter_housing') ?>"
                                                     data-num="<?php the_field('field_home_counter_housing') ?>">0
                                                </div>
                                            </div>
                                        </div>
                                        <h6><?php _e('Logements', 'lienard'); ?></h6>
                                    </div>
                                    <!-- 3 -->
                                    <div class="inline-facts col-lg-6">
                                        <div class="milestone-counter">
                                            <div class="stats animaper d-lg-flex">
                                                <div class="num" style="width: auto;"
                                                     data-content="<?php the_field('field_home_counter_square_foot') ?>"
                                                     data-num="<?php the_field('field_home_counter_square_foot') ?>">
                                                    0
                                                </div>
                                                <span style="width: auto;">000</span>
                                            </div>
                                        </div>
                                        <h6><?php _e('pi2 aires de vie commune intérieure', 'lienard'); ?></h6>
                                    </div>
                                </div>
                                <!-- facts   end -->
                            </div>
                        </div>
                    </div>
                    <!--  container  end -->
                </section>
            </div>

            <!-- content  -->
            <div class="content alternating-sections">
                <section>
                    <div class="container">
                        <div class="row presentation-first align-items-center">
                            <div class="col-lg-5">
                                <h2 class="section-title"><?php the_field('field_home_presentation_title_top'); ?></h2>
                                <?php the_field('field_home_presentation_text_top'); ?>
                                <?php $link = get_field('field_home_presentation_button_top');
                                if ($link):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <a class=" btn anim-button   flat-btn   transition"
                                       href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                                class="fa fa-long-arrow-right"></i></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-7">

                                <img src="<?php the_field('field_home_presentation_image_top'); ?>" class="respimg"
                                     alt="">

                            </div>
                        </div>
                    </div>

                    <div class="sect-subtitle right-align-dec" data-top-bottom="transform: translateY(200px);"
                         data-bottom-top="transform: translateY(-200px);"><span>03</span></div>
                    <div class="container">
                        <div class="row presentation-second align-items-center">
                            <div class="col-lg-7 order-lg-1 order-2">

                                <img src="<?php the_field('field_home_presentation_image_bottom'); ?>" class="respimg"
                                     alt="">
                                <div class="text-on-image"><div class="page"><?php the_field('field_home_presentation_text_image_bottom'); ?></div></div>
                            </div>
                            <div class="col-lg-5 order-lg-2 order-1">
                                <h2 class="section-title"><?php the_field('field_home_presentation_title_bottom'); ?></h2>
                                <?php the_field('field_home_presentation_text_bottom'); ?>
                                <?php $link = get_field('field_home_presentation_button_bottom');
                                if ($link):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <a class=" btn anim-button   flat-btn   transition  fl-l"
                                       href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                                class="fa fa-long-arrow-right"></i></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>


            <div class="content fullwidth-section">
                <!-- parallax image  -->
                <div class="parallax-inner">
                    <div class="bg" data-bg="<?php the_field('field_home_image_fullscreen'); ?>"
                         data-top-bottom="transform: translateY(300px);"
                         data-bottom-top="transform: translateY(-300px);"></div>
                </div>
            </div>

            <div class="content alternating-sections">
                <section>

                    <div class="sect-subtitle left-align-dec" data-top-bottom="transform: translateY(-200px);"
                         data-bottom-top="transform: translateY(200px);"><span>04</span></div>
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-5">
                                <h2 class="section-title"><?php the_field('field_home_plan_title'); ?></h2>
                                <?php the_field('field_home_plan_text'); ?>
                                <?php $link = get_field('field_home_plan_button');
                                if ($link):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <a class=" btn anim-button   flat-btn   transition"
                                       href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                                class="fa fa-long-arrow-right"></i></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-7">

                                <img src="<?php the_field('field_home_plan_image'); ?>" class="respimg"
                                     alt="">

                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content gallery">
                <section>

                        <!--=============== portfolio holder ===============-->
                        <div class="gallery-items   three-columns grid-small-pad lightgallery">
                            <?php
                            $images = get_field('field_home_gallery');
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)
                            if ($images): ?>
                                <?php foreach ($images as $image): ?>
                                    <div class="gallery-item">
                                        <div class="grid-item-holder">
                                            <div class="box-item">
                                                <a data-src="<?php echo esc_url($image['url']); ?>" class="popup-image"
                                                   data-sub-html="<?php echo esc_html($image['caption']); ?>">
                                                    <span class="overlay"></span>
                                                    <img src="<?php echo esc_url($image['url']); ?>" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <!-- end gallery items -->
                        <!-- custom-link-holder  end -->
                    <div class="button-gallery">
                        <?php $link = get_field('field_home_gallery_button');
                        if ($link):
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a class=" btn anim-button   flat-btn   transition"
                               href="<?php echo esc_url($link_url); ?>"
                               target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                        class="fa fa-long-arrow-right"></i></a>
                        <?php endif; ?>
                    </div>
                </section>

            </div>
            <?php include 'templates/includes/content-footer.php'; ?>
            <!-- content end -->
        </div>

    </div><!-- .content-area -->

<?php
get_footer();
