<footer class="fixed-footer">
    <div class="footer-social">
        <ul>
            <li><a href="<?php the_field('field_footer_social_facebook','option');?>" target="_blank" ><i class="fa fa-facebook"></i></a></li>
            <li><a href="<?php the_field('field_footer_social_instagram','option');?>" target="_blank" ><i class="fa fa-instagram"></i></a></li>
        </ul>
    </div>
    <!-- Header  title -->
    <div class="footer-title">
        <?php
        $link = get_field('field_header_link','option');
        if( $link ):
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
            <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
        <?php endif; ?>
    </div>
    <!-- Header  title  end-->
</footer>
<span class="to-top"></span>
<?php
