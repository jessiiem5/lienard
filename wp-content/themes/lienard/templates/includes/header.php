<header>
    <!-- header-inner  -->
    <div class="header-inner">
        <!-- header logo -->
        <div class="logo-holder">
            <a href="<?php echo get_home_url(); ?>"><img src="<?php the_field('logo','option'); ?>" alt=""></a>
        </div>
        <!-- header logo end -->
        <!-- mobile nav button -->
        <div class="nav-button-holder">
            <div class="nav-button vis-m"><span></span><span></span><span></span></div>
        </div>
        <!-- mobile nav button end -->
        <!-- navigation  -->
        <div class="nav-holder">
            <nav class="scroll-nav">
                <?php
                wp_nav_menu( array(
                'menu'           => 'main_menu_fr', // Do not fall back to first non-empty menu.
                'fallback_cb'    => false // Do not fall back to wp_page_menu()
                ) );
                ?>
            </nav>
        </div>
        <!-- navigation  end -->
    </div>
    <!-- header-inner  end -->
</header>
<?php
