<div class="height-emulator">
    <div id="to-top-stop"></div>
</div>
<footer class="content-footer">
    <!--  container  -->
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-6">
                <!-- Footer logo -->
                <div class="footer-item footer-logo">
                    <h4><?php the_field('field_footer_newsletter_title', 'option'); ?></h4>
                    <p><?php the_field('field_footer_newsletter_text', 'option'); ?></p>
                    <?php echo do_shortcode('[contact-form-7 id="' . get_field('field_footer_newsletter_form', 'option') . '"]'); ?>
                </div>
                <!-- Footer logo end -->
            </div>
            <!-- Footer info -->
            <div class="col-xl-6 offset-xl-2 col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="footer-item">
                            <h5 class="text-link"><?php _e('Téléphone', 'lienard'); ?></h5>
                            <ul>
                                <li>
                                    <a href="tel:<?php the_field('field_footer_telephone', 'option'); ?>"><?php the_field('field_footer_telephone', 'option'); ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Footer info end-->
                    <!-- Footer info -->
                    <div class="col-lg-4">
                        <div class="footer-item">
                            <h5 class="text-link"><?php _e('Adresse', 'lienard'); ?></h5>
                            <p><?php the_field('field_footer_address', 'option') ?></p>
                        </div>
                    </div>
                    <!-- Footer info-->
                    <!-- Footer info end-->
                    <div class="col-lg-4">
                        <div class="footer-item">
                            <h5 class="text-link"><?php _e('Horaire', 'lienard'); ?></h5>
                            <p><?php the_field('field_footer_hours', 'option'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="footer-wrap">
                                        <span class="copyright"><?php _e('Tous droits réservés &#169; 2020 - Liénard ', 'lienard') ?>
                                        </span>
                            <span><a href="https://snabb.ca"><?php _e('Une création Snabb', 'lienard'); ?></a></span>

                        </div>
                    </div>
                </div>
            </div>


            <!-- Footer info end-->
        </div>
        <!-- Footer copyright -->


        <!-- Footer copyright end -->
    </div>
    <!--  container  end -->
</footer>