<?php
/**
 * Template Name: Map
 * Description: A Page Template for the visite page.
 */
get_header();
?>


    <div id="wrapper" class="page-content">
        <div class="content-holder">

            <!-- content  -->
            <!-- Page title -->
            <div class="dynamic-title"><h1><?php the_title() ?></h1></div>
            <!-- Page title  end-->
            <!-- content  -->
            <div class="content background-header"
                 style="background-image:url('<?php the_field('field_default_background_image') ?>')">
                <div class="overlay"></div>
                <section>
                    <div class="container">
                        <div class="page-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?php the_title() ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content map">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="imagemappro">
                                <?php the_content(); ?>
                            </div>
                            <div class="accordion_mobile">
                                <p><?php the_field('accordion_text'); ?></p>
                                <div class="module-content-accordion">
                                    <?php
                                    $etages = get_field('accordion_mobile');
                                    if ($etages): ?>
                                        <?php foreach ($etages as $etage): ?>
                                            <div>
                                                <div class="accordion-plan btn-link d-flex justify-content-center align-items-center">
                                                    <?php echo $etage['accordion_title']; ?>
                                                </div>
                                                <div class="panel">
                                                    <?php
                                                    $unites = $etage['accordion_unites'];
                                                    if ($unites): ?>
                                                        <?php foreach ($unites as $unite): ?>
                                                        <div>
                                                            <p><?php echo $unite['accordion_unites_number']; ?></p>
                                                            <p><?php echo $unite['accordion_unites_rooms']; ?></p>
                                                            <a target="_blank" href="<?php echo $unite['accordion_unites_plan']['url']; ?>">Voir le plan</a>
                                                        </div>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?php include 'templates/includes/content-footer.php'; ?>
            <!-- content end -->
        </div>

    </div><!-- .content-area -->

<?php
get_footer();
