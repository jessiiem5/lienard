<?php

get_header();
?>


    <div id="wrapper" class="page-content">
        <div class="content-holder">

            <!-- content  -->
            <!-- Page title -->
            <div class="dynamic-title"><h1><?php the_title() ?></h1></div>
            <!-- Page title  end-->
            <!-- content  -->
            <div class="content background-header"
                 style="background-image:url('<?php the_field('field_default_background_image') ?>')">
                <div class="overlay"></div>
                <section>
                    <div class="container">
                        <div class="page-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?php the_title() ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content default">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>


            <?php include 'templates/includes/content-footer.php'; ?>
            <!-- content end -->
        </div>

    </div><!-- .content-area -->

<?php
get_footer();
