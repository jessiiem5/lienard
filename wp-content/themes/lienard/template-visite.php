<?php
/**
 * Template Name: Visite
 * Description: A Page Template for the visite page.
 */
get_header();
?>


    <div id="wrapper" class="page-content">
        <div class="content-holder">

            <!-- content  -->
            <!-- Page title -->
            <div class="dynamic-title"><h1><?php the_title() ?></h1></div>
            <!-- Page title  end-->
            <!-- content  -->
            <div class="content background-header" style="background-image:url('<?php the_field('field_visite_background_image') ?>')">
                <div class="overlay"></div>
                <section>
                    <div class="container">
                        <div class="page-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?php the_title() ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content visite-intro">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="section-title"><?php the_field('field_visite_intro_title'); ?></h2>
                            <?php the_field('field_visite_intro_text'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            $unites = get_field('visite_unites_repeater');
            if ($unites): ?>
                <?php foreach ($unites as $index => $unite): ?>
                    <div class="content alternating-sections unite">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-lg-4 order-lg-<?php echo ($index + 1) % 2 ?> order-2">
                                    <h2 class="section-title"><?php echo $unite['unite_title']; ?></h2>
                                    <h3><?php echo $unite['unite_subtitle']; ?></h3>
                                    <?php echo $unite['unite_list']; ?>
                                    <?php $link = $unite['unite_button'];
                                    if ($link):
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                        ?>
                                        <a class=" btn anim-button   flat-btn   transition"
                                           href="<?php echo esc_url($link_url); ?>"
                                           target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                                    class="fa fa-long-arrow-right"></i></a>
                                    <?php endif; ?>
                                </div>
                                <div class="col-lg-8 video">

                                    <div class="module-video-thumbnail">
                                        <div id="thumbnail" class="d-flex align-items-center justify-content-center"
                                             style="background-image: url('<?php echo $unite['unite_thumbnail']; ?>');">
                                            <?php
                                            $videoID = "";
                                            $videoURL = $unite['unite_video'];
                                            if (strpos($videoURL, 'www.youtube.com') !== false) {
                                                $videoID = explode('?v=', $videoURL)[1];
                                            } else {
                                                $videoID = explode('.be/', $videoURL)[1];
                                            }
                                            ?>

                                            <div class="media-video__video"
                                                 data-videoid="<?php echo $videoID ?>"></div>
                                            <a class="video-play-trigger"><img
                                                        src="<?php echo get_template_directory_uri() ?>/assets/svg/play-btn.svg"/></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="content galerie" id="section-galerie">
                <h2 class="section-title"><strong><?php the_field('field_visite_gallery_title'); ?></strong></h2>
                    <div class="single-slider-holder lightgallery" data-looped="0">
                        <div class="single-slider">
                            <?php
                            $images = get_field('visite_gallery');
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)
                            if ($images): ?>
                                <?php foreach ($images as $image): ?>
                                    <div class="item">
                                        <img src="<?php echo esc_url($image['url']); ?>" alt="">
                                        <div class="under">
                                            <p class="legende"><?php echo esc_html($image['title']); ?></p>
                                            <p class="description"><?php echo esc_html($image['caption']); ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <div class="customNavigation ssn">
                            <a class="prev-slide transition"></a>
                            <a class="next-slide transition"></a>
                        </div>
                    </div>
            </div>


            <?php include 'templates/includes/content-footer.php'; ?>
            <!-- content end -->
        </div>

    </div><!-- .content-area -->

<?php
get_footer();
