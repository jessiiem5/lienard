<?php
/**
 * Template Name: Commodités
 * Description: A Page Template for the commodities page.
 */
get_header();
?>


    <div id="wrapper" class="page-content">
        <div class="content-holder">

            <!-- content  -->
            <!-- Page title -->
            <div class="dynamic-title"><h1><?php the_title() ?></h1></div>
            <!-- Page title  end-->
            <!-- content  -->
            <div class="content background-header" style="background-image:url('<?php the_field('commodities_background_image'); ?>');">
                <div class="overlay"></div>
                <section>
                    <div class="container">
                        <div class="page-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?php the_title() ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content services">
                <section>
                    <!--  container  -->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="section-title"><?php the_field('commodities_services_title'); ?></h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="module-content-accordion">

                                            <?php
                                            $services = get_field('commodities_services_repeater_left');
                                            if ($services): ?>
                                                <?php foreach ($services as $service): ?>
                                                    <div>
                                                        <div class="accordion btn-link d-flex justify-content-between align-items-center">
                                                            <?php echo $service['commodities_services_repeater_title']; ?>
                                                            <i class="fa fa-angle-down"></i>
                                                        </div>
                                                        <div class="panel">
                                                            <p><?php echo $service['commodities_services_repeater_text']; ?></p>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="module-content-accordion">

                                            <?php
                                            $services = get_field('commodities_services_repeater_right');
                                            if ($services): ?>
                                                <?php foreach ($services as $service): ?>
                                                    <div>
                                                        <div class="accordion btn-link d-flex justify-content-between align-items-center">
                                                            <?php echo $service['commodities_services_repeater_title']; ?>
                                                            <i class="fa fa-angle-down"></i>
                                                        </div>
                                                        <div class="panel">
                                                            <p><?php echo $service['commodities_services_repeater_text']; ?></p>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  container end  -->
                </section>
            </div>

            <div class="content loisirs">
                <div class="container">
                    <div class="row presentation-first justify-content-center">
                        <div class="col-12">
                            <h2 class="section-title"><?php the_field('field_commodities_loisirs_title'); ?></h2>
                            <div class="bg-grey">
                                <?php the_field('field_commodities_loisirs_list'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content outside">
                <section>
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-7">

                                <img src="<?php the_field('field_commodities_outside_image'); ?>" class="respimg"
                                     alt="">

                            </div>
                            <div class="col-lg-5 texte">
                                <h2 class="section-title"><?php the_field('field_commodities_outside_title'); ?></h2>
                                <?php the_field('field_commodities_outside_list'); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content cta">
                <div class="container">
                    <div class="row align-items-center justify-content-center flex-column">
                        <h2><?php the_field('field_commodities_cta_title'); ?></h2>
                        <?php $link = get_field('field_commodities_cta_button');
                        if ($link):
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a class=" btn anim-button   flat-btn   transition"
                               href="<?php echo esc_url($link_url); ?>"
                               target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                        class="fa fa-long-arrow-right"></i></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php include 'templates/includes/content-footer.php'; ?>
            <!-- content end -->
        </div>

    </div><!-- .content-area -->

<?php
get_footer();
