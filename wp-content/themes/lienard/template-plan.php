<?php
/**
 * Template Name: Plan
 * Description: A Page Template for the plan page.
 */
get_header();
?>


    <div id="wrapper" class="page-content">
        <div class="content-holder">

            <!-- content  -->
            <!-- Page title -->
            <div class="dynamic-title"><h1><?php the_title() ?></h1></div>
            <!-- Page title  end-->
            <!-- content  -->
            <div class="content background-header" style="background-image:url('<?php the_field('plan_background_image'); ?>');">
                <div class="overlay"></div>
                <section>
                    <div class="container">
                        <div class="page-title">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?php the_title() ?></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="content plan" id="sec2">
                <section>
                    <!--  container  -->
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-7">
                                <div class="module-video-thumbnail">
                                    <div id="thumbnail" class="d-flex align-items-center justify-content-center"
                                         style="background-image: url('<?php the_field('field_plan_thumbnail'); ?>');">
                                        <?php
                                        $videoID = "";
                                        $videoURL = get_field('field_plan_video');
                                        if (strpos($videoURL, 'www.youtube.com') !== false) {
                                            $videoID = explode('?v=', $videoURL)[1];
                                        } else {
                                            $videoID = explode('.be/', $videoURL)[1];
                                        }
                                        ?>

                                        <div class="media-video__video"
                                             data-videoid="<?php echo $videoID ?>"></div>
                                        <a class="video-play-trigger"><img src="<?php echo get_template_directory_uri() ?>/assets/svg/play-btn.svg" /></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!-- section title  -->
                                <h2 class="section-title"><?php the_field('field_plan_title'); ?></h2>
                                <!-- section title  end -->
                                <?php the_field('field_plan_text'); ?>
                                <?php $link = get_field('field_plan_button');
                                if ($link):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                    <a class="btn anim-button fl-l" href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><span><?php echo esc_html($link_title); ?></span><i
                                                class="fa fa-long-arrow-right"></i></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!--  container end  -->
                </section>
            </div>

            <?php include 'templates/includes/content-footer.php'; ?>
            <!-- content end -->
        </div>

    </div><!-- .content-area -->

<?php
get_footer();
