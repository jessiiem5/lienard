<?php
/**
 * Register and Enqueue Styles.
 */
require_once(get_template_directory() . '/fields/CustomFields.php');
require_once(get_template_directory() . '/fields/Home.php');
require_once(get_template_directory() . '/fields/Commodities.php');
require_once(get_template_directory() . '/fields/Contact.php');
require_once(get_template_directory() . '/fields/Quartier.php');
require_once(get_template_directory() . '/fields/Visite.php');
require_once(get_template_directory() . '/fields/Plan.php');
require_once(get_template_directory() . '/fields/Map.php');
require_once(get_template_directory() . '/fields/Defaut.php');


add_action('wp_enqueue_scripts', 'lienard_register_assets');
add_action('after_setup_theme', 'lienard_register_menus', 0);
add_action('acf/init', ['CustomFields', 'init_custom_fields']);
add_action('acf/init', ['Home', 'init_home']);
add_action('acf/init', ['Commodities', 'init_commodities']);
add_action('acf/init', ['Contact', 'init_contact']);
add_action('acf/init', ['Quartier', 'init_quartier']);
add_action('acf/init', ['Visite', 'init_visite']);
add_action('acf/init', ['Plan', 'init_plan']);
add_action('acf/init', ['Map', 'init_map']);
add_action('acf/init', ['Defaut', 'init_defaut']);
add_action('init', 'register_settings');

function lienard_register_assets()
{

    wp_enqueue_style('lienard-css', get_template_directory_uri() . '/assets/style.css', null, '0.1');

    wp_deregister_script('jquery');

    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', null, '0.1', true);

    wp_enqueue_script('lienard-assets-js', get_template_directory_uri() . '/assets/js/assets.js', null, '0.1', true);

    wp_enqueue_script('lienard-js', get_template_directory_uri() . '/assets/js/script.js', null, '0.1.0', true);

    wp_enqueue_script('gmaps', 'https://maps.googleapis.com/maps/api/js?key=' . get_field('field_google_map_api_key', 'option'), false, false, false);

    wp_enqueue_script('lienard-maps', get_template_directory_uri() . '/assets/js/maps.js', null, '0.1', true);
}

function lienard_register_menus()
{
    register_nav_menus(array(
            'main_menu_fr' => __('Menu principal', 'lienard'),
        )
    );
}

/**
 * Where you register custom settings page
 */
function register_settings()
{
    if (function_exists('acf_add_options_page')) {
        acf_add_options_page(array(
                'page_title' => 'Paramètres généraux',
                'menu_title' => 'Paramètres généraux',
                'menu_slug' => 'parameters',
            )
        );
    }
}